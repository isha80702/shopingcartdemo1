package sheridan;

public class PaymentServiceFactory {
    private static PaymentServiceFactory instance = null;
    
    private PaymentServiceFactory( ) {
        
    }
    
    public PaymentService getPaymentService( PaymentServiceType type ) {
        switch ( type ) {
            case CREDIT : return new CreditPaymentService( );
            case DEBIT : return new DebitPaymentService( );
        }
        return null;
    }
    
    
    public static PaymentServiceFactory getInstance( ) {
        if ( instance == null ) {
            instance = new PaymentServiceFactory( );
        }
        return instance;
    }
}
