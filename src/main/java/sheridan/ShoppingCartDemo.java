package sheridan;

public class ShoppingCartDemo {
    
    public static void main( String args [ ] ) {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance( );
        PaymentService creditService = factory.getPaymentService( PaymentServiceType.CREDIT );
        PaymentService debitService = factory.getPaymentService( PaymentServiceType.DEBIT );        
            
        Cart cart = new Cart( );
        cart.addProduct( new Product( "shirt" , 50 ) );
        cart.addProduct( new Product( "pants" , 60 ) );
           
        cart.setPaymentService( creditService );        
        cart.payCart();
           
        cart.setPaymentService( debitService );    
        cart.payCart();        
        
    } 

    static boolean validateProduct(String product) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    
}
