package sheridan;

public enum PaymentServiceType {
    CREDIT, DEBIT;
}
