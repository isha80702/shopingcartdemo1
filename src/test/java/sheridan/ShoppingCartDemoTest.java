/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Dell
 */
public class ShoppingCartDemoTest {
    
    public ShoppingCartDemoTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void GoodTest()
    {
        System.out.println("Testing Regular: ");
        String product = "Shirt";
        ShoppingCartDemo instance = new ShoppingCartDemo();
        boolean expResult = true;
        boolean result = ShoppingCartDemo.validateProduct(product);
        assertEquals(expResult, result);
        fail("Not an item");
    }
    @Test
    public void BadTest()
    {
        System.out.println("Testing Regular: ");
        String product = "L@ptops";
        ShoppingCartDemo instance = new ShoppingCartDemo();
        boolean expResult = true;
        assertEquals("Bad Expression, Please select a valid product",expResult);
    }
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        ShoppingCartDemo.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
